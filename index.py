from flask import Flask
app = Flask(__name__)

import redis
r = redis.StrictRedis(host='localhost', port=6379, db=0)
from flask import request
import geocoder

@app.route('/geocode', methods=['POST'])
def geocode():
	js = request.get_json()
	text = js['text']
	fetched = r.get(text)
	if ( fetched is not None) :

		return "{'result': " + fetched + "}"
	else:
		g = geocoder.arcgis(text)
		r.set(text, g.latlng)
		return "{'result': " + str(g.latlng) + "}"
